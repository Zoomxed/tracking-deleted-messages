#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  2 00:30:37 2018
@author: zooma
"""
import vk_requests
import requests
import json
from datetime import datetime


api = vk_requests.create_api(service_token='how to get token see readme', interactive=True, api_version='5.84')
messages = []

def getNameById(id):
    r = api.users.get(user_ids=id)
    name = '{} {}'.format(r[0].get('first_name'), r[0].get('last_name'))
    return name
    
serv = api.messages.getLongPollServer(need_pts=0, lp_version=3)
TS = serv['ts']

while 1:
    r = requests.post('https://{}?act=a_check&key={}&ts={}&wait=25&mode=2&version=3 '.format(serv['server'], serv['key'], TS))
    res = json.loads(r.text)
    if res['updates'] != []:
        if res['updates'][0][0] == 4:
            messages += [[res['updates'][0][1],
                         datetime.utcfromtimestamp(res['updates'][0][4]).strftime('%Y-%m-%d %H:%M:%S'),
                         res['updates'][0][5]]]
            
        if res['updates'][0][0] == 2:
            for i in messages:
                if i[0] == res['updates'][0][1]:
                    print('deleted from {}: {}'.format(getNameById(res['updates'][0][3]), i))
                        
    TS = res['ts']